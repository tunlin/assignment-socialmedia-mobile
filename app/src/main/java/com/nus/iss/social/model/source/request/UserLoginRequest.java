package com.nus.iss.social.model.source.request;

public class UserLoginRequest {
    public String email;
    public String password;

    public UserLoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}

