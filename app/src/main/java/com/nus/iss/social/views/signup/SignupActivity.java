package com.nus.iss.social.views.signup;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nus.iss.social.R;
import com.nus.iss.social.common.LogUtils;
import com.nus.iss.social.common.activities.ActivityBase;
import com.nus.iss.social.common.logger.Log;
import com.nus.iss.social.model.source.Usermanager;
import com.nus.iss.social.model.source.request.UserSignupResponse;
import com.nus.iss.social.views.friends.FriendSearchActivity;

public class SignupActivity extends ActivityBase {

    private static final String TAG = LogUtils.makeLogTag(SignupActivity.class);

    private AutoCompleteTextView emailView;
    private EditText nameView;
    private EditText passwordView;
    private EditText phoneView;

    private View progressView;
    private View loginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_);

        emailView       = findViewById(R.id.email);
        nameView        = findViewById(R.id.name);
        phoneView       = findViewById(R.id.phonenumber);
        passwordView    = findViewById(R.id.password);

        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button signupButton = (Button) findViewById(R.id.signup_button);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        loginFormView = findViewById(R.id.signup_button);
        progressView = findViewById(R.id.signup_progress);

        emailView.setText("wintunlin_1@gmail.com");
        nameView.setText("Tun Lin");
        phoneView.setText("94574943");
        passwordView.setText("password123");

        // Check application has user_ID there
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String user_id  = defaultSharedPreferences.getString("user_id", "");
        String email    = defaultSharedPreferences.getString("email", "");
        String password    = defaultSharedPreferences.getString("password", "");


        if (!TextUtils.isEmpty(email)) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("email", email);
            intent.putExtra("password", password);

            startActivity(intent);
            finish();
        }
    }

    private void attemptLogin() {

        // Reset errors.
        emailView.setError(null);
        nameView.setError(null);
        phoneView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        final String email        =  emailView.getText().toString();
        String name         = nameView.getText().toString();
        String phoneNumber  = nameView.getText().toString();
        final String password     = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(name)) {
            nameView.setError(getString(R.string.error_field_required));
            focusView = nameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(phoneNumber)) {
            phoneView.setError(getString(R.string.error_field_required));
            focusView = phoneView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;

        } else if (!isEmailValid(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;

        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            //

            String token = FirebaseInstanceId.getInstance().getToken();
            Usermanager.getInstance().signUp(email, name, phoneNumber, password, token,
                new Usermanager.TaskCompleteListener<UserSignupResponse>() {

                @SuppressLint("CommitPrefEdits")
                @Override
                public void onCompleted(UserSignupResponse response) {

                    Log.d(TAG, "onCompleted: " + response.toString());

                    final String user_id = response.user_id;

                    Log.i(TAG, "User ID : " + user_id);

                    SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignupActivity.this);
                    SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                    edit.putString("user_id", user_id);
                    edit.putString("email", email);
                    edit.putString("password", password);
                    edit.commit();

                    Context context = SignupActivity.this;
                    Intent intent = new Intent(context, FriendSearchActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                    finish();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgress(false);


                        }
                    });
                }

                @Override
                public void onError(Exception e) {
                    Log.e(TAG, "onError: " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgress(false);
                            emailView.setError(getString(R.string.error_invalid_email));
                            emailView.requestFocus();
                        }
                    });
                }
            });

        }


    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

