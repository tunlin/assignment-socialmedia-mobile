package com.nus.iss.social.views.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nus.iss.social.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class FriendSearchFragment extends Fragment implements View.OnClickListener{

    EditText searchEditText;
    private Button showAllButton;
    private Button searchButton;

    public FriendSearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_search, container, false);


        searchEditText = view.findViewById(R.id.searchEditText);
        showAllButton = view.findViewById(R.id.showAllButton);
        searchButton = view.findViewById(R.id.searchButton);

        showAllButton.setOnClickListener(this);
        searchButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.searchButton :

                searchUser(getActivity());
                break;
            case R.id.showAllButton:
                // showAllUser(getActivity());

                Intent intent = new Intent(getActivity(), FriendListActivity.class);
                startActivity(intent);

                break;
            default:
                break;
        }
    }

    private void searchUser(final Context activity) {

        String searchUsername = searchEditText.getText().toString();


        if (TextUtils.isEmpty(searchUsername)) {
            searchEditText.setError(getString(R.string.error_field_required));
            return;
        }


    }

    private void showAllUser(final Context activity) {


    }
}



