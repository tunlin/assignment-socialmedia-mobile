package com.nus.iss.social.model.source.request;

public class UserSignupRequest {

    public String email;
    public String name;
    public String phone_no;
    public String password;
    public String push_id;


    public UserSignupRequest(String email, String name, String phonenumber, String password, String token) {

        this.email = email;
        this.name = name;
        this.phone_no = phonenumber;
        this.password = password;
        this.push_id = token;

    }
}

