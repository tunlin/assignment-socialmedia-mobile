package com.nus.iss.social.model.source.request;

public class UserSignupResponse {
    public String user_id;

    public UserSignupResponse() {
    }

    public UserSignupResponse(String user_id) {
        this.user_id = user_id;
    }
}
