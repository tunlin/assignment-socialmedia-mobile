package com.nus.iss.social.views;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.nus.iss.social.R;
import com.nus.iss.social.common.LogUtils;
import com.nus.iss.social.common.activities.ActivityBase;
import com.nus.iss.social.common.logger.Log;
import com.nus.iss.social.model.source.UserDetail;
import com.nus.iss.social.model.source.Usermanager;
import com.nus.iss.social.util.ActivityUtils;
import com.nus.iss.social.views.feeds.FeedsFragment;
import com.nus.iss.social.views.friends.FriendListFragment;
import com.nus.iss.social.views.profile.ProfileFragment;

public class MainActivity extends ActivityBase {

    private static String TAG = LogUtils.makeLogTag(MainActivity.class);

    private String user_id;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()) {
                case R.id.navigation_feed:
                    setTitle(R.string.title_feeds);

                    Fragment feedFragment = FeedsFragment.newInstance(1);
                    ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), feedFragment, R.id.main_Content);

                    return true;
                case R.id.navigation_friends:
                    setTitle(R.string.title_friends);

                    Fragment friendListFragment = FriendListFragment.newInstance(1);
                    ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), friendListFragment, R.id.main_Content);

                    return true;
                case R.id.navigation_profile:
                    setTitle(R.string.title_profile);

                    Fragment profileFragment = ProfileFragment.newInstance();
                    ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), profileFragment, R.id.main_Content);

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent() != null) {
            user_id = getIntent().getStringExtra("user_id");
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Fragment feedFragment = FeedsFragment.newInstance(1);
        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), feedFragment, R.id.main_Content);

        Usermanager.getInstance().getAllUsers(new Usermanager.TaskCompleteListener<UserDetail[]>() {
            @Override
            public void onCompleted(UserDetail[] data) {
                Log.e(TAG, "Users : " + data.toString());
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "User get all : " + e.getMessage());
            }
        });

        Usermanager.getInstance().getUserById(user_id, new Usermanager.TaskCompleteListener<UserDetail>() {
            @Override
            public void onCompleted(UserDetail data) {
                Log.e(TAG, "User : " + data);
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "User get id : " + e.getMessage());
            }
        });

        /*
        IdentityManager defaultIdentityManager = IdentityManager.getDefaultIdentityManager();
        defaultIdentityManager.addSignInStateChangeListener(new SignInStateChangeListener() {
            @Override
            public void onUserSignedIn() {
                Log.i(TAG, "User sign in ");
            }

            @Override
            public void onUserSignedOut() {
                Log.i(TAG, "User sign out ");
            }
        });

        boolean userSignedIn = defaultIdentityManager.isUserSignedIn();
        Log.i(TAG, "User sign in : " + userSignedIn);

        String cachedUserID = defaultIdentityManager.getCachedUserID();
        Log.i(TAG, "Cached User id : " + cachedUserID);

        String userPoolId       = "ap-southeast-1_Iv83Si6bo";
        String clientId         = "3nl5j6e53av8s0uda5qvbt9nll";
        String clientSecret     = "1h4be66af9bom6rrt97ut5u0qpdoi3ids91qp2n3g4k3v8pffj0g";
        String cognitoRegion    = "ap-southeast-1";


        CognitoUserPool userPool = new CognitoUserPool(getApplicationContext(), userPoolId, clientId, clientSecret, Regions.fromName(cognitoRegion));
        CognitoUser currentUser = userPool.getCurrentUser();
        currentUser.getDetailsInBackground(new GetDetailsHandler() {
            @Override
            public void onSuccess(CognitoUserDetails cognitoUserDetails) {
                CognitoUserAttributes attributes = cognitoUserDetails.getAttributes();

                Log.i(TAG, "User detail : " + attributes.getAttributes().toString());
            }

            @Override
            public void onFailure(Exception exception) {

                Log.e(TAG, "Get detail Failure : " + exception.getMessage());
            }
        });

        // Instantiate a AmazonDynamoDBMapperClient
        AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient(AWSMobileClient.getInstance().getCredentialsProvider());
        final DynamoDBMapper dynamoDBMapper = DynamoDBMapper.builder()
            .dynamoDBClient(dynamoDBClient)
            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
            .build();

        new Thread(new Runnable() {
            @Override
            public void run() {
                // Create News
                // final String cachedIdentityId = IdentityManager.getDefaultIdentityManager().getCachedUserID();
                // String userID = cachedIdentityId;

                // Log.d(TAG, "Cache identity ID : " + cachedIdentityId);

                final NewsDO newsItem = new NewsDO();
                newsItem.setUserId("user_01");

                newsItem.setArticleId("Article1");
                newsItem.setContent("This is the article content");
                dynamoDBMapper.save(newsItem);

                // NewsDO newsItem2 = dynamoDBMapper.load(NewsDO.class, userID,"Article1");
                // dynamoDBMapper.save(newsItem);
                // dynamoDBMapper.delete(newsItem);
            }
        }).start();

        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_logout :

                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                defaultSharedPreferences.edit().clear().commit();

                return true;
                default:
                    return super.onOptionsItemSelected(item);
        }

    }
}
