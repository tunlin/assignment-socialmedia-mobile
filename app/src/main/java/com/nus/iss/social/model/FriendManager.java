package com.nus.iss.social.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FriendManager {

    public static final List<Friend> ITEMS = new ArrayList<Friend>();

    public static final Map<String, Friend> ITEM_MAP = new HashMap<String, Friend>();

    private static final int COUNT = 5;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Friend item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static Friend createDummyItem(int position) {
        return new Friend("ID : " + String.valueOf(position), "Name : ");
    }



}

