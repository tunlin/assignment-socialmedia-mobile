package com.nus.iss.social.views.friends;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nus.iss.social.R;
import com.nus.iss.social.model.Friend;
import com.nus.iss.social.views.friends.FriendListFragment.OnListFragmentInteractionListener;

import java.util.List;

public class FriendListViewAdapter extends RecyclerView.Adapter<FriendListViewAdapter.ViewHolder> {

    private final List<Friend> values;
    private final OnListFragmentInteractionListener listener;

    public FriendListViewAdapter(List<Friend> items, OnListFragmentInteractionListener listener) {
        values = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.fragment_friend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.item = values.get(position);
        holder.idView.setText(values.get(position).id);
        holder.nameTextView.setText(values.get(position).name);
        holder.followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onListFragmentInteraction(holder.item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView idView;
        public final TextView nameTextView;
        public final Button followButton;

        public Friend item;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            idView = (TextView) view.findViewById(R.id.idTextView);
            nameTextView = (TextView) view.findViewById(R.id.nameTextView);
            followButton =  view.findViewById(R.id.followButton);
        }


    }
}
