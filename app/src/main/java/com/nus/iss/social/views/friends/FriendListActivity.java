package com.nus.iss.social.views.friends;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nus.iss.social.R;
import com.nus.iss.social.model.Friend;
import com.nus.iss.social.model.source.Usermanager;
import com.nus.iss.social.util.ActivityUtils;
import com.nus.iss.social.views.MainActivity;

public class FriendListActivity extends AppCompatActivity implements FriendListFragment.OnListFragmentInteractionListener {

    private String user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        if (getIntent() != null) {
            user_id = getIntent().getStringExtra("user_id");
        }


        Button doneButton =  findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FriendListActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Fragment friendFollowFragment = FriendListFragment.newInstance(1);
        ActivityUtils.addFragmentToActivity(
            getSupportFragmentManager(),
            friendFollowFragment, R.id.friendListContent);

        Usermanager.getInstance().getAllUsers(new Usermanager.TaskCompleteListener() {
            @Override
            public void onCompleted(Object data) {

            }

            @Override
            public void onError(Exception e) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onListFragmentInteraction(Friend item) {
        Toast.makeText(this, item.toString(), Toast.LENGTH_LONG).show();
    }
}
