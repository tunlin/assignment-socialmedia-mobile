package com.nus.iss.social.model.source;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserDetail {

    @SerializedName("user_id")
    @Expose
    public String userId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone_no")
    @Expose
    public String phoneNo;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("push_id")
    @Expose
    public String pushId;

    @SerializedName("uploaded_photo_id")
    @Expose
    public List<String> uploadedPhotoId = new ArrayList<>();

    @SerializedName("liked_photo_id")
    @Expose
    public List<String> likedPhotoId = new ArrayList<>();

    @SerializedName("followed_friend_id")
    @Expose
    public List<String> followedFriendId = new ArrayList<>();

    @SerializedName("created_timestamp")
    @Expose
    public String createdTimestamp;

    @Override
    public String toString() {
        return "UserDetail{" +
            "userId='" + userId + '\'' +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", phoneNo='" + phoneNo + '\'' +
            ", pushId='" + pushId + '\'' +
            ", uploadedPhotoId=" + uploadedPhotoId +
            ", likedPhotoId=" + likedPhotoId +
            ", followedFriendId=" + followedFriendId +
            ", createdTimestamp='" + createdTimestamp + '\'' +
            '}';
    }
}
