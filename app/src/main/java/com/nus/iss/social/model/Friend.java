package com.nus.iss.social.model;

public class Friend {
    public final String id;
    public final String name;

    public Friend(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Friend{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            '}';
    }
}
