package com.nus.iss.social.model.source;

import java.util.ArrayList;
import java.util.List;

public class PhotoInfo {
    public String photoId;
    public String uploadedUserId;
    public List<String> liked_user_ids = new ArrayList<>();
    public double created_timestamp;
    public String original_photo_id;
    public String thumbnail_photo_id;
}
