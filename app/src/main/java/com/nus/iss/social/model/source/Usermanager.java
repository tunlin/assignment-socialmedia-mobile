package com.nus.iss.social.model.source;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nus.iss.social.common.LogUtils;
import com.nus.iss.social.common.logger.Log;
import com.nus.iss.social.model.source.request.UserLoginRequest;
import com.nus.iss.social.model.source.request.UserLoginResponse;
import com.nus.iss.social.model.source.request.UserSignupRequest;
import com.nus.iss.social.model.source.request.UserSignupResponse;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Thomas Win on 20/5/18.
 */
public class Usermanager {

    public static final String URL_GET_USER_ALL = "https://opwhgi6ya4.execute-api.ap-southeast-1.amazonaws.com/Prod/user/all";
    public static final String URL_USER_DETAIL = "https://opwhgi6ya4.execute-api.ap-southeast-1.amazonaws.com/Prod/user/";

    public interface TaskCompleteListener<T> {

        void onCompleted(T data);
        void onError(Exception e);
    }
    private static final String TAG = LogUtils.makeLogTag(Usermanager.class);

    public static final String URL_SIGN_IN  = "https://opwhgi6ya4.execute-api.ap-southeast-1.amazonaws.com/Prod/user/signin";
    public static final String URL_SIGNUP   = "https://opwhgi6ya4.execute-api.ap-southeast-1.amazonaws.com/Prod/user/signup";

    private static Usermanager INSTANCE = null;


    public static Usermanager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Usermanager();
        }
        return INSTANCE;
    }

    private Usermanager() {}

    public void signIn(String email, String password, final TaskCompleteListener<UserLoginResponse> listener) {

        UserLoginRequest userLoginRequest = new UserLoginRequest(email, password);
        String payload = new Gson().toJson(userLoginRequest);

        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, payload);
        Request request = new Request.Builder()
            .url(URL_SIGN_IN)
            .post(body)
            .addHeader("content-type", "application/json")
            .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                int code = response.code();
                Log.i(TAG, "Response : " + code);

                if (code == 200 || code == 201) {

                    String responseStr = response.body().string();
                    Log.i(TAG, "Response : " + responseStr);

                    try {

                        UserLoginResponse loginRespone = new Gson().fromJson(responseStr, UserLoginResponse.class);
                        listener.onCompleted(loginRespone);

                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        listener.onError(new Exception("Error : " + responseStr));
                    }

                } else {
                    listener.onError(new Exception("Code : " + code));
                }
            }
        });
    }


    public void signUp(String email,
                       String name,
                       String phoneNumber,
                       String password,
                       String token, final TaskCompleteListener<UserSignupResponse> taskCompleteListener) {

        UserSignupRequest signupRequest = new UserSignupRequest(email, name, phoneNumber, password, token);
        String payload = new Gson().toJson(signupRequest);

        Log.i(TAG, "Sign up request : " + payload);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, payload);

        Request request = new Request.Builder()
            .url(URL_SIGNUP)
            .post(body)
            .addHeader("content-type", "application/json")
            .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                taskCompleteListener.onError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                int code = response.code();
                Log.i(TAG, "Response : " + code);

                if (code == 200 || code == 201) {

                    String responseStr = response.body().string();
                    Log.i(TAG, "Response : " + responseStr);

                    try {
                        UserSignupResponse userSignupResponse = new Gson().fromJson(responseStr, UserSignupResponse.class);
                        taskCompleteListener.onCompleted(userSignupResponse);

                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        taskCompleteListener.onError(new Exception("Error : " + responseStr));
                    }

                } else {
                    taskCompleteListener.onError(new Exception("Code : " + code));
                }
            }
        });
    }

    public void getUserById(String userId, final TaskCompleteListener<UserDetail> taskCompleteListener) {

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
            .url(URL_USER_DETAIL + userId)
            .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                taskCompleteListener.onError(e);

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int code = response.code();
                Log.i(TAG, "Response : " + code);

                if (code == 200 || code == 201) {

                    String responseStr = response.body().string();
                    Log.i(TAG, "Response : " + responseStr);

                    try {

                        Gson gson = new Gson();
                        UserDetail user = gson.fromJson(responseStr, UserDetail.class);
                        taskCompleteListener.onCompleted(user);

                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        taskCompleteListener.onError(new Exception("Error : " + responseStr));
                    }

                } else {
                    taskCompleteListener.onError(new Exception("Code : " + code));
                }
            }
        });

    }

    public void getAllUsers(final TaskCompleteListener<UserDetail[] > taskCompleteListener) {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
            .url(URL_GET_USER_ALL)
            .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                taskCompleteListener.onError(e);

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int code = response.code();
                Log.i(TAG, "Response : " + code);

                if (code == 200 || code == 201) {

                    String responseStr = response.body().string();
                    Log.i(TAG, "Response : " + responseStr);

                    try {

                        Gson gson = new Gson();
                        UserDetail[] users = gson.fromJson(responseStr, UserDetail[].class);
                        taskCompleteListener.onCompleted(users);

                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                        taskCompleteListener.onError(new Exception("Error : " + responseStr));
                    }

                } else {
                    taskCompleteListener.onError(new Exception("Code : " + code));
                }
            }
        });
    }
}

