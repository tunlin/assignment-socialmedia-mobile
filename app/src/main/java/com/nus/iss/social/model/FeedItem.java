package com.nus.iss.social.model;

public class FeedItem {
    public final String id;
    public final String content;
    public final String details;

    public FeedItem(String id, String content, String details) {
        this.id = id;
        this.content = content;
        this.details = details;
    }

    @Override
    public String toString() {
        return content;
    }
}
