package com.nus.iss.social;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobile.auth.core.IdentityHandler;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.auth.ui.AuthUIConfiguration;
import com.amazonaws.mobile.auth.ui.SignInUI;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.nus.iss.social.common.LogUtils;
import com.nus.iss.social.common.logger.Log;
import com.nus.iss.social.views.MainActivity;

import java.io.File;

/**
 * A placeholder fragment containing a simple view.
 */
public class DeveloperFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(DeveloperFragment.class);

    public DeveloperFragment() {


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_developer, container, false);

        view.findViewById(R.id._signUpButton).setOnClickListener(this);
        view.findViewById(R.id._loginButton).setOnClickListener(this);
        view.findViewById(R.id._awsSetupButton).setOnClickListener(this);
        view.findViewById(R.id._awsTestButton).setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id._loginButton:
                login(getActivity());
                break;

            case R.id._signUpButton:
                break;

            case R.id._awsSetupButton:
                setupAWS(getActivity());
                break;

            case R.id._awsTestButton:
                break;

            default:
                break;
        }
    }

    private void login(final Context context) {

        Log.i(TAG, "Login User");

        AWSMobileClient.getInstance().initialize(context, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {

                AuthUIConfiguration config =
                    new AuthUIConfiguration.Builder()
                        .userPools(true)  // true? show the Email and Password UI
                        // .signInButton(FacebookButton.class) // Show Facebook button
                        // .signInButton(GoogleButton.class) // Show Google button
                        // .logoResId(R.drawable.mylogo) // Change the logo
                        // .backgroundColor(Color.BLUE) // Change the backgroundColor
                        // .isBackgroundColorFullScreen(true) // Full screen backgroundColor the backgroundColor full screenff
                        .fontFamily("sans-serif-light") // Apply sans-serif-light as the global font
                        .canCancel(true)
                        .build();

                SignInUI signin = (SignInUI) AWSMobileClient.getInstance().getClient(context, SignInUI.class);
                signin.login((Activity) context, MainActivity.class)
                    .authUIConfiguration(config)
                    .execute();
            }
        }).execute();

    }

    private void setupAWS(final Context context) {

        Log.i(TAG, "SETUP AWS");



        AWSMobileClient.getInstance().initialize(context, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {

                Log.d(TAG, "AWSMobileClient is instantiated and you are connected to AWS!");
                Toast.makeText(context, "Initialized done", Toast.LENGTH_LONG).show();

                AWSCredentialsProvider credentialsProvider = AWSMobileClient.getInstance().getCredentialsProvider();
                AWSConfiguration configuration = AWSMobileClient.getInstance().getConfiguration();

                // Log.d(TAG, "Configuration : " + configuration.toString());
                // Log.d(TAG, "CredentialsProvider : " + credentialsProvider.toString());

                // Use IdentityManager#getUserID to fetch the identity id.
                IdentityManager.getDefaultIdentityManager().getUserID(new IdentityHandler() {
                    @Override
                    public void onIdentityId(String identityId) {

                        Log.d(TAG, "Identity ID = " + identityId);

                        final String cachedIdentityId = IdentityManager.getDefaultIdentityManager().getCachedUserID();
                        Log.d(TAG, "Cache User identity ID : " + cachedIdentityId);
                    }

                    @Override
                    public void handleError(Exception exception) {
                        Log.d(TAG, "Error in retrieving the identity" + exception);
                    }
                });
            }
        }).execute();
    }


    private void uploadFile(Context context) {

        AWSMobileClient.getInstance().initialize(context).execute();

        TransferUtility transferUtility =
            TransferUtility.builder()
                .context(context)
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                .build();

        TransferObserver uploadObserver =
            transferUtility.upload(
                "s3Folder/s3Key.txt",
                new File("/path/to/file/localFile.txt"));

        // Attach a listener to the observer to get state update and progress notifications
        uploadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                    + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == uploadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d("YourActivity", "Bytes Transferrred: " + uploadObserver.getBytesTransferred());
        Log.d("YourActivity", "Bytes Total: " + uploadObserver.getBytesTotal());


        TransferObserver downloadObserver =
            transferUtility.download(
                "s3Folder/s3Key.txt",
                new File("/path/to/file/localFile.txt"));

        // Attach a listener to the observer to get state update and progress notifications
        downloadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float)bytesCurrent/(float)bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d("MainActivity", "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == downloadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d("YourActivity", "Bytes Transferrred: " + downloadObserver.getBytesTransferred());
        Log.d("YourActivity", "Bytes Total: " + downloadObserver.getBytesTotal());
    }

}
