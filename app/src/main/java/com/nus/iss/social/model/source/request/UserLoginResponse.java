package com.nus.iss.social.model.source.request;

public class UserLoginResponse {

    public String user_id;

    public UserLoginResponse() {
    }

    public UserLoginResponse(String user_id) {
        this.user_id = user_id;
    }
}
